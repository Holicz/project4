#!/usr/bin/env python
import pika
import mysql.connector

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

cnx = mysql.connector.connect(user='root', password='', host='127.0.0.1', database='dk.analyzer')

channel.queue_declare(queue='domain_to_save', durable=True)


def callback(ch, method, properties, domain):
    # Do not run on dk-hostmaster.dk
    if 'dk-hostmaster.dk' in domain:
        print(" [!] Skipping dk-hostmaster.dk: '" + domain + "'")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    print(" [x] Saving domain '" + domain + "'")

    cursor = cnx.cursor()
    cursor.execute('INSERT IGNORE INTO domain (domain) VALUES (%s)', (domain,))
    cursor.execute('INSERT IGNORE INTO information (domain) VALUES (%s)', (domain,))
    cnx.commit()

    print(" [+] Successfully saved domain (or it already exists): '" + domain + "' to database.")

    cursor.close()

    ch.basic_ack(delivery_tag=method.delivery_tag)
    return


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue='domain_to_save')

print(' [*] Waiting for domains. To exit press CTRL+C')
channel.start_consuming()
