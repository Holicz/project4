#!/usr/bin/env python
import string
import pika
from urllib2 import urlopen
from bs4 import BeautifulSoup
import tldextract
import re

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue='url_to_scrape', durable=True)
channel.queue_declare(queue='url_to_process', durable=True)
channel.queue_declare(queue='url_to_save', durable=True)
channel.queue_declare(queue='domain_to_save', durable=True)


def callback(ch, method, properties, url):
    print(" [x] Processing url '" + url + "'")

    # Skip not valid URLs
    if not string.split(url, ';')[0] or not string.split(url, ';')[1]:
        print(" [!] At least on part of this URL is empty: '" + url + "'")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    # Skip unwanted file types
    short_extension = (string.split(url, ';')[1][-4:]).lower()
    long_extension = (string.split(url, ';')[1][-5:]).lower()
    short_extension_wo_params = (((string.split(url, ';')[1]).split('?')[0])[-4:]).lower()
    long_extension_wo_params = (((string.split(url, ';')[1]).split('?')[0])[-5:]).lower()
    skipped_short_extensions = ['.jpg', '.png', '.gif', '.bmp', '.pdf', '.doc', '.xls', '.ppt', '.txt', '.exe', '.dmg',
                                '.zip', '.rar', '.mp3', '.mp4']
    skipped_long_extensions = ['.jpeg', '.webp', '.webm', '.docx', '.xlsx', '.pptx', '.ashx']

    if short_extension in skipped_short_extensions or long_extension in skipped_long_extensions:
        print(" [!] Target URL is unwanted file type: '" + url + "'")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    # Skip "#"
    if re.match('^[#]*$', string.split(url, ';')[1]):
        print(" [!] Url is adding only char '#'")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    extracted_url = tldextract.extract(string.split(url, ';')[1])

    if not extracted_url.domain or not extracted_url.suffix:
        # Here can end up URL like "/register" so we try to add it to original domain/URL and check validity again

        if string.split(url, ';')[1][0] == '/':
            # If this is true the link path is absolute and ve should append it only to domain, not to whole URL
            extracted_origin_url = tldextract.extract(string.split(url, ';')[0])

            if not extracted_origin_url.domain or not extracted_origin_url.suffix:
                print(" [!] Not a valid origin URL for absolute path URL: '" + url + "'")
                ch.basic_ack(delivery_tag=method.delivery_tag)
                return

            url = extracted_origin_url.domain + '.' + extracted_origin_url.suffix + string.split(url, ';')[1]

        else:
            # For relative path
            url = string.split(url, ';')[0] + string.split(url, ';')[1]

        extracted_url = tldextract.extract(url)

        if not extracted_url.domain or not extracted_url.suffix:
            print(" [!] Not a valid url: '" + url + "'")
            ch.basic_ack(delivery_tag=method.delivery_tag)
            return

    else:
        url = string.split(url, ';')[1]

    if extracted_url.suffix != 'dk':
        print (" [-] Not a .dk domain: '" + url + "'")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    domain = extracted_url.domain + '.' + extracted_url.suffix

    # Do not run on dk-hostmaster.dk
    if 'dk-hostmaster.dk' in domain:
        print(" [!] Skipping dk-hostmaster.dk: '" + domain + "'")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    channel.basic_publish(exchange='', routing_key='url_to_save', body=url,
                          properties=pika.BasicProperties(delivery_mode=2))
    print(" [+] Queue 'url_to_save': add '" + url + "'")

    channel.basic_publish(exchange='', routing_key='domain_to_save', body=domain,
                          properties=pika.BasicProperties(delivery_mode=2))
    print(" [+] Queue 'domain_to_save': add '" + domain + "'")

    ch.basic_ack(delivery_tag=method.delivery_tag)
    return


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue='url_to_process')

print(' [*] Waiting for URLs. To exit press CTRL+C')
channel.start_consuming()
