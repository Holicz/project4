#!/usr/bin/env python
import pika
import mysql.connector
import hashlib

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

cnx = mysql.connector.connect(user='root', password='', host='127.0.0.1', database='dk.analyzer')

channel.queue_declare(queue='url_to_save', durable=True)


def callback(ch, method, properties, url):
    # Do not run on dk-hostmaster.dk
    if 'dk-hostmaster.dk' in url:
        print(" [!] Skipping dk-hostmaster.dk: '" + url + "'")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    print(" [x] Saving url '" + url + "'")

    try:
        cursor = cnx.cursor()
        cursor.execute('INSERT INTO url (url_hash, url) VALUES (UNHEX(%s), %s)', (hashlib.sha1(url).hexdigest(), url))
        cnx.commit()

        channel.basic_publish(exchange='', routing_key='url_to_scrape', body=url,
                              properties=pika.BasicProperties(delivery_mode=2))
        print(" [+] Queue 'url_to_scrape': add '" + url + "'")

        cursor.close()
    except mysql.connector.Error as e:
        print(" [-] Url already scraped: '" + url + "'")

    ch.basic_ack(delivery_tag=method.delivery_tag)
    return


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue='url_to_save')

print(' [*] Waiting for URLs. To exit press CTRL+C')
channel.start_consuming()
