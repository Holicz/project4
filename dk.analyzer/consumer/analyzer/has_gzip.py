# !/usr/bin/env python

import pika
import json
import urllib2, httplib

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue='has_gzip', durable=True)
channel.queue_declare(queue='information_to_save', durable=True)


def callback(ch, method, properties, domain):
    print(" [x] Checking Gzip compression for domain '" + domain + "'")

    try:
        url = 'http://' + domain
        request = urllib2.Request(url)
        request.add_header('Accept-encoding', 'gzip')
        response = urllib2.urlopen(request)
        response_headers = response.info()

        if 'Content-encoding' in response_headers and 'gzip' in response_headers['Content-encoding']:
            has_gzip = 1
            print(" [+] Site '" + domain + "' is compressed with Gzip.")
        else:
            has_gzip = 0
            print(" [+] Site '" + domain + "' is NOT compressed with Gzip.")

        data = {
            'information_type': 'gzip',
            'information_value': has_gzip,
            'domain': domain,
            'success': 1
        }

        channel.basic_publish(
            exchange='',
            routing_key='information_to_save',
            body=json.dumps(data),
            properties=pika.BasicProperties(delivery_mode=2))
    except:
        print(" [!] An error occured when checking Gzip compression for domain '" + domain + "'")

        data = {
            'information_type': 'gzip',
            'information_value': None,
            'domain': domain,
            'success': 0
        }

        channel.basic_publish(
            exchange='',
            routing_key='information_to_save',
            body=json.dumps(data),
            properties=pika.BasicProperties(delivery_mode=2))

    ch.basic_ack(delivery_tag=method.delivery_tag)
    return


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue='has_gzip')

print(' [*] Waiting for domains to analyze. To exit press CTRL+C')
channel.start_consuming()
