# !/usr/bin/env python

import pika
import json
import socket
import ssl

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue='has_http2', durable=True)
channel.queue_declare(queue='information_to_save', durable=True)


# @see https://blog.mafr.de/2016/08/27/detecting-http2-support/
def callback(ch, method, properties, domain):
    print(" [x] Checking HTTP/2 for domain '" + domain + "'")

    ctx = ssl.create_default_context()
    ctx.set_alpn_protocols(['h2', 'http/1.1'])

    try:
        url = 'www.' + domain
        conn = ctx.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM), server_hostname=url)
        conn.connect((url, 443))

        if conn.selected_alpn_protocol() == 'h2':
            has_http2 = 1
            print(" [+] Site '" + domain + "' is running over HTTP/2.")
        else:
            has_http2 = 0
            print(" [+] Site '" + domain + "' is NOT running over HTTP/2.")

        data = {
            'information_type': 'http2',
            'information_value': has_http2,
            'domain': domain,
            'success': 1
        }

        channel.basic_publish(
            exchange='',
            routing_key='information_to_save',
            body=json.dumps(data),
            properties=pika.BasicProperties(delivery_mode=2))
    # except ssl.CertificateError:
    #     if 'www.' not in domain:
    #         channel.basic_publish(
    #             exchange='',
    #             routing_key='has_http2',
    #             body='www.' + domain,
    #             properties=pika.BasicProperties(delivery_mode=2))
    #
    #         print(" [!] Domain will be tried again with 'www.'")
    except:
        print(" [!] An error occured when checking HTTP/2 for domain '" + domain + "'")

        data = {
            'information_type': 'http2',
            'information_value': None,
            'domain': domain,
            'success': 0
        }

        channel.basic_publish(
            exchange='',
            routing_key='information_to_save',
            body=json.dumps(data),
            properties=pika.BasicProperties(delivery_mode=2))

    ch.basic_ack(delivery_tag=method.delivery_tag)
    return


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue='has_http2')

print(' [*] Waiting for domains to analyze. To exit press CTRL+C')
channel.start_consuming()
