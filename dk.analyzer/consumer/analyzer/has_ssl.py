# !/usr/bin/env python

import requests
import pika
import json

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue='has_ssl', durable=True)
channel.queue_declare(queue='information_to_save', durable=True)


def callback(ch, method, properties, domain):
    print(" [x] Checking SSL for domain '" + domain + "'")

    url = 'https://' + domain

    try:
        response = requests.get(url)

        print(" [+] Site '" + domain + "' is running over SSL.")

        data = {
            'information_type': 'ssl',
            'information_value': 1,
            'domain': domain,
            'success': 1
        }

        channel.basic_publish(
            exchange='',
            routing_key='information_to_save',
            body=json.dumps(data),
            properties=pika.BasicProperties(delivery_mode=2))

    except requests.exceptions.ConnectionError:
        print(" [+] Site '" + domain + "' is not using SSL.")

        data = {
            'information_type': 'ssl',
            'information_value': 0,
            'domain': domain,
            'success': 1
        }

        channel.basic_publish(
            exchange='',
            routing_key='information_to_save',
            body=json.dumps(data),
            properties=pika.BasicProperties(delivery_mode=2))

    except:
        print(" [!] An error occured when checking SSL for domain '" + domain + "'")

        data = {
            'information_type': 'ssl',
            'information_value': None,
            'domain': domain,
            'success': 0
        }

        channel.basic_publish(
            exchange='',
            routing_key='information_to_save',
            body=json.dumps(data),
            properties=pika.BasicProperties(delivery_mode=2))

    ch.basic_ack(delivery_tag=method.delivery_tag)
    return


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue='has_ssl')

print(' [*] Waiting for domains to analyze. To exit press CTRL+C')
channel.start_consuming()
