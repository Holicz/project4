#!/usr/bin/env python
import pika
import mysql.connector
import json

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

cnx = mysql.connector.connect(user='root', password='', host='127.0.0.1', database='dk.analyzer')

channel.queue_declare(queue='information_to_save', durable=True)


def callback(ch, method, properties, message):
    data = json.loads(message)

    print(" [x] Saving information for domain '" + data['domain'] + "'")

    cursor = cnx.cursor()
    cursor.execute(
        'UPDATE information SET `' + data['information_type'] + '` = %s, last_try_success = %s WHERE domain = %s',
        (data['information_value'], data['success'], data['domain'],))
    cnx.commit()

    print(" [+] Successfully saved information for domain: '" + data['domain'] + "' to the database.")

    cursor.close()

    ch.basic_ack(delivery_tag=method.delivery_tag)
    return


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue='information_to_save')

print(' [*] Waiting for domains. To exit press CTRL+C')
channel.start_consuming()
