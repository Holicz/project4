#!/usr/bin/env python

import pika
import urllib2, httplib
import json


class HeadRequest(urllib2.Request):
    def get_method(self):
        return "HEAD"


class NonsenseRequest(urllib2.Request):
    def get_method(self):
        return 'NONSENSE'


connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue='guess_webserver', durable=True)
channel.queue_declare(queue='information_to_save', durable=True)


def callback(ch, method, properties, domain):
    print(" [x] Guessing webserver for domain '" + domain + "'")

    url = 'http://' + domain

    httplib.HTTPConnection._http_vsn = 11
    httplib.HTTPConnection._http_vsn_str = 'HTTP/1.1'
    try:
        response = urllib2.urlopen(HeadRequest(url), timeout=10)
        response_headers = response.info()
        headers = response_headers.headers
        server = {}

        server_name = None
        # Step 1: Try to get Server header and it's value - 40 points
        try:
            if response_headers['Server']:
                server_name = (response_headers['Server'].split('/')[0]).lower()
                server[server_name] = 40
        except:
            pass

        # Step 2: Check order of headers - 20 points
        # If date before server, it's probably Apache
        # If Content-Type before Content-Length, it's probably nginx
        date_index = False
        server_index = False
        content_type_index = False
        content_length_index = False

        for header in headers:
            if header.startswith('Date'):
                date_index = headers.index(header)
            elif header.startswith('Server'):
                server_index = headers.index(header)
            elif header.startswith('Content-Length'):
                content_length_index = headers.index(header)
            elif header.startswith('Content-Type'):
                content_type_index = headers.index(header)

        try:
            if date_index < server_index:
                if 'apache' in server:
                    server['apache'] += 20
                else:
                    server['apache'] = 20
        except:
            pass

        try:
            if content_type_index < content_length_index:
                if 'nginx' in server:
                    server['nginx'] += 20
                else:
                    server['nginx'] = 20
        except:
            pass

        # Step 3: Send unsupported method (NONSENSE) and compare the answer - 20 points
        try:
            response = urllib2.urlopen(NonsenseRequest(url), timeout=10)
        except urllib2.HTTPError, e:
            if str(e) == 'HTTP Error 405: Not Allowed':
                if 'nginx' in server:
                    server['nginx'] += 20
                else:
                    server['nginx'] = 20
            elif str(e) == 'HTTP Error 501: Not Implemented':
                if 'apache' in server:
                    server['apache'] += 20
                else:
                    server['apache'] = 20
            elif str(e) == 'HTTP Error 401: Unauthorized':
                if 'microsoft-iis' in server:
                    server['microsoft-iis'] += 20
                else:
                    server['microsoft-iis'] = 20
        except:
            pass

        # Step 4: Send unsupported HTTP version (HTTP/4.0) - 20 points
        # Apache returns 200
        # Nginx returns 200 or 505 :(
        # IIS returns 505
        httplib.HTTPConnection._http_vsn = 40
        httplib.HTTPConnection._http_vsn_str = 'HTTP/4.0'

        try:
            response = urllib2.urlopen(HeadRequest(url), timeout=10)

            if response.getcode() == 200:
                if 'apache' in server:
                    server['apache'] += 20
                else:
                    server['apache'] = 20

                if 'nginx' in server:
                    server['nginx'] += 20
                else:
                    server['nginx'] = 20
        except urllib2.HTTPError, e:
            if str(e) == 'HTTP Error 505: HTTP Version Not Supported':
                if 'microsoft-iis' in server:
                    server['microsoft-iis'] += 20
                else:
                    server['microsoft-iis'] = 20

                if 'nginx' in server:
                    server['nginx'] += 20
                else:
                    server['nginx'] = 20
        except:
            pass

        # Step 5: Try NULL character in URL - 20 points
        # Apache returns 404
        # Nginx returns 400
        # IIS returns 400
        try:
            response = urllib2.urlopen(HeadRequest(url + '/%00'), timeout=10)
        except urllib2.HTTPError, e:
            if e.getcode() == 404:
                if 'apache' in server:
                    server['apache'] += 20
                else:
                    server['apache'] = 20

        guess_server = 'Could not guess'
        success = 0

        if server_name in locals():
            guess_server = server_name
        else:
            for webserver, score in server.iteritems():
                if score >= 60:
                    guess_server = webserver
                    success = 1

        data = {
            'information_type': 'webserver',
            'information_value': guess_server,
            'domain': domain,
            'success': success
        }

        if success:
            print(" [+] Webserver for domain '" + domain + "' is '" + guess_server + "'")
        else:
            print(" [!] Could not guess Webserver for domain '" + domain + "'")

        channel.basic_publish(
            exchange='',
            routing_key='information_to_save',
            body=json.dumps(data),
            properties=pika.BasicProperties(delivery_mode=2))

    except:
        data = {
            'information_type': 'webserver',
            'information_value': None,
            'domain': domain,
            'success': 0
        }

        print(" [!] An error occured when guessing a webserver for domain '" + domain + "'")

        channel.basic_publish(
            exchange='',
            routing_key='information_to_save',
            body=json.dumps(data),
            properties=pika.BasicProperties(delivery_mode=2))

    ch.basic_ack(delivery_tag=method.delivery_tag)
    return


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue='guess_webserver')

print(' [*] Waiting for domains to analyze. To exit press CTRL+C')
channel.start_consuming()
