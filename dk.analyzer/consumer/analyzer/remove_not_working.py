#!/usr/bin/env python
import pika
import mysql.connector
import json
import urllib2


class HeadRequest(urllib2.Request):
    def get_method(self):
        return "HEAD"


connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

cnx = mysql.connector.connect(user='root', password='', host='127.0.0.1', database='dk.analyzer')

channel.queue_declare(queue='remove_not_working', durable=True)


def callback(ch, method, properties, url):
    print(" [x] Checking domain '" + url + "'")

    try:
        response = urllib2.urlopen(HeadRequest('http://' + url), timeout=10)

        print(" [+] Domain '" + url + "' is ok.")
    except:
        print(" [x] Domain '" + url + "' is not working. Removing from database.")

        cursor = cnx.cursor()
        cursor.execute('DELETE FROM information WHERE `domain` = "' + url + '"')
        cnx.commit()
        cursor.close()

    ch.basic_ack(delivery_tag=method.delivery_tag)
    return

channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue='remove_not_working')

print(' [*] Waiting for URLs. To exit press CTRL+C')
channel.start_consuming()
