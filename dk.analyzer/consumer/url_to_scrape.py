#!/usr/bin/env python

import httplib
import pika
import urllib2
from urllib2 import urlopen
from bs4 import BeautifulSoup
import ssl

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue='url_to_scrape', durable=True)
channel.queue_declare(queue='url_to_process', durable=True)


class HeadRequest(urllib2.Request):
    def get_method(self):
        return "HEAD"


def callback(ch, method, properties, url):
    # Do not run on dk-hostmaster.dk
    if 'dk-hostmaster.dk' in url:
        print(" [!] Skipping dk-hostmaster.dk: '" + url + "'")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    url = url.rstrip('/')  # Remove trailing slash if any

    # Add protocol if needed
    scheme, address = urllib2.splittype(url)

    if not scheme:
        url = 'http://' + url

    print(" [x] Scraping url '" + url + "'")

    # Remove anchors
    url = url.split('#')[0]

    # Skip images, documents, and other unwanted files
    # We also need to check if extension is not before parameters to exclude URLs like "http://exmaple.com/img.jpg?v=1"
    short_extension = (url[-4:]).lower()
    long_extension = (url[-5:]).lower()
    short_extension_wo_params = ((url.split('?')[0])[-4:]).lower()
    long_extension_wo_params = ((url.split('?')[0])[-5:]).lower()
    skipped_short_extensions = ['.jpg', '.png', '.gif', '.bmp', '.pdf', '.doc', '.xls', '.ppt', '.txt', '.exe', '.dmg',
                                '.zip', '.rar', '.mp3', '.mp4']
    skipped_long_extensions = ['.jpeg', '.webp', '.webm', '.docx', '.xlsx', '.pptx', '.ashx']

    if short_extension in skipped_short_extensions or short_extension_wo_params in skipped_short_extensions or long_extension in skipped_long_extensions or long_extension_wo_params in skipped_long_extensions:
        print(" [!] Target URL is unwanted file type: '" + url + "'")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    try:
        response = urllib2.urlopen(HeadRequest(url), timeout=10)
        response_headers = response.info()
    except:
        print (" [-] Unknown error!")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    # Skip pages which give us file to download
    if 'Content-Disposition' in response_headers and len(response_headers['Content-Disposition']) >= 10 and \
            response_headers['Content-Disposition'][0:10] == 'attachment':
        print(" [!] Page is responding with file: '" + url + "'")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    # Skip everything what is not HTML page
    if 'Content-Type' in response_headers and 'text/html' not in response_headers['Content-Type']:
        print(" [!] Page is responding with unwanted content type ('" + response_headers['Content-Type'] + "'): '" + url + "'")
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    try:
        site = urlopen(url, timeout=10).read()
        soup_site = BeautifulSoup(site, 'html.parser')

        # HTML code can contain something like this and it affects absolute paths
        # <BASE href="http://example.com/foobar/">
        if soup_site.find('base') and soup_site.find('base').get('href'):
            base_url = soup_site.find('base').get('href')
        else:
            base_url = url

        for link in soup_site.find_all('a'):
            if not link.get('href') is None:
                channel.basic_publish(
                    exchange='',
                    routing_key='url_to_process',
                    body=(base_url + ';' + link.get('href')).lower(),
                    properties=pika.BasicProperties(delivery_mode=2))

                print(" [+] Queue 'url_to_process': add '" + (link.get('href')).lower() + "'")
    except urllib2.HTTPError as e:
        print (" [-] HTTP Error! '" + e.message + "'")
    except urllib2.URLError as e:
        print (" [-] URL Syntax Error! '" + e.message + "'")
    except ssl.CertificateError as e:
        print (" [-] SSL Error! '" + e.message + "'")
    except UnicodeDecodeError as e:
        print (" [-] Unicode Decode Error! '" + e.message + "'")
    except httplib.BadStatusLine as e:
        print (" [-] Bad Status Line Error! '" + e.message + "'")
    except:
        print (" [-] Unknown error!")

    ch.basic_ack(delivery_tag=method.delivery_tag)
    return


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue='url_to_scrape')

print(' [*] Waiting for URLs. To exit press CTRL+C')
channel.start_consuming()
