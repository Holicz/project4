#!/usr/bin/env python

import pika
import mysql.connector

cnx = mysql.connector.connect(user='root', password='', host='127.0.0.1', database='dk.analyzer')

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue='guess_webserver', durable=True)
channel.queue_declare(queue='has_ssl', durable=True)
channel.queue_declare(queue='has_http2', durable=True)
channel.queue_declare(queue='has_gzip', durable=True)
channel.queue_declare(queue='remove_not_working', durable=True)

cursor = cnx.cursor()

# # Add domains to queue "remove_not_working"
# cursor.execute('SELECT domain FROM information')
#
# domains = cursor.fetchall()
#
# for domain in domains:
#     channel.basic_publish(
#         exchange='',
#         routing_key='remove_not_working',
#         body=domain[0],
#         properties=pika.BasicProperties(delivery_mode=2))
#
#     print(" [+] Domain '" + domain[0] + "' put into Remove not working queue")
#
# # Add domains to the queue "guess_webserver"
# cursor.execute('SELECT domain FROM information WHERE `webserver` IS NULL')
#
# domains = cursor.fetchall()
#
# for domain in domains:
#     channel.basic_publish(
#         exchange='',
#         routing_key='guess_webserver',
#         body=domain[0],
#         properties=pika.BasicProperties(delivery_mode=2))
#
#     print(" [+] Domain '" + domain[0] + "' put into Guess webserver queue")
#
# # Add domains to queue "has_ssl"
# cursor.execute('SELECT domain FROM information WHERE `ssl` IS NULL')
#
# domains = cursor.fetchall()
#
# for domain in domains:
#     channel.basic_publish(
#         exchange='',
#         routing_key='has_ssl',
#         body=domain[0],
#         properties=pika.BasicProperties(delivery_mode=2))
#
#     print(" [+] Domain '" + domain[0] + "' put into Has SSL queue")
#
# # Add domains to queue "has_http2"
# # HTTP2 can be ran only with SSL, so we can skip those sites which does not have an SSL
# cursor.execute('UPDATE information SET `http2` = 0 WHERE `ssl` = 0')
# cursor.execute('SELECT domain FROM information WHERE `http2` IS NULL')
#
# domains = cursor.fetchall()
#
# for domain in domains:
#     channel.basic_publish(
#         exchange='',
#         routing_key='has_http2',
#         body=domain[0],
#         properties=pika.BasicProperties(delivery_mode=2))
#
#     print(" [+] Domain '" + domain[0] + "' put into Has HTTP/2 queue")
#
# Add domains to queue "has_gzip"
cursor.execute('SELECT domain FROM information WHERE `gzip` IS NULL')

domains = cursor.fetchall()

for domain in domains:
    channel.basic_publish(
        exchange='',
        routing_key='has_gzip',
        body=domain[0],
        properties=pika.BasicProperties(delivery_mode=2))

    print(" [+] Domain '" + domain[0] + "' put into Has Gzip queue")